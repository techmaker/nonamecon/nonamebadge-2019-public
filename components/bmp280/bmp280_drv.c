/**
*	TechMaker
*	https://techmaker.ua
*
*	ESP32 driver for BMP280 chip with ESP-IDF library
*	based on Bosch Sensortec GmbH driver version 2.0.5
*	based on Bosch Sensortec GmbH datasheet rev. 1.14 5/15/2015 (BST-BMP280-DS001-11)
*	13 Sep 2019 by Alexander Olenyev <sasha@techmaker.ua>
*
*	Changelog:
*		- v1.0 initial release for ESP32 ESP-IDF using I2C interface (SPI not yet supported)
*/

#include "bmp280_drv.h"

#define ACK_CHECK_EN 0x1   // I2C master will check ack from slave
#define ACK_CHECK_DIS 0x0  // I2C master will not check ack from slave
#define ACK 0x0			   // I2C ack value
#define NACK 0x1		   // I2C nack value

void BMP280_Delay(uint32_t ms) {
	return vTaskDelay(ms / portTICK_PERIOD_MS);
}

/**************************************************************/
/**\name	I2C BUS COMMUNICATION CUSTOM FUNCTIONS */
/**************************************************************/

static i2c_port_t i2c_port;

void BMP280_I2C_Set(i2c_port_t port) {
	i2c_port = port;
}

void BMP280_I2C_Init(i2c_port_t port, gpio_num_t scl_io_num, gpio_num_t sda_io_num, uint32_t clk_speed) {
	i2c_config_t conf = {
		.mode = I2C_MODE_MASTER,
		.scl_io_num = scl_io_num,
		.scl_pullup_en = GPIO_PULLUP_ENABLE,
		.sda_io_num = sda_io_num,
		.sda_pullup_en = GPIO_PULLUP_ENABLE,
		.master.clk_speed = clk_speed,
	};
	i2c_port = port;
	i2c_param_config(i2c_port, &conf);
	i2c_driver_install(i2c_port, conf.mode, 0, 0, 0);
}
/*!
 * @brief: The function is used as I2C bus write
 *
 *
 *
 *
 *	@param dev_addr : The device address of the sensor
 *	@param reg_addr : Address of the first register, where data is to be written
 *	@param reg_data : It is a value held in the array, which is written in the register
 *	@param cnt : The no of bytes of data to be written
 *
 *
 *	@return status of the I2C write
 *
 *
 */
/**
 * @brief I2C write to register function
 *
 * ___________________________________________________________________________________
 * | start | slave_addr + wr_bit + ack | reg_addr + ack | write n bytes + ack | stop |
 * --------|---------------------------|----------------|---------------------|------|
 *
 */
int8_t BMP280_I2C_Write(uint8_t dev_addr, uint8_t reg_addr, uint8_t *reg_data, uint8_t cnt) {
	i2c_cmd_handle_t cmd = i2c_cmd_link_create();
	i2c_master_start(cmd);
	i2c_master_write_byte(cmd, (dev_addr << 1) | I2C_MASTER_WRITE, ACK_CHECK_EN);
	i2c_master_write_byte(cmd, reg_addr, ACK_CHECK_EN);
	i2c_master_write(cmd, (uint8_t *)reg_data, cnt, ACK_CHECK_EN);
	i2c_master_stop(cmd);
	esp_err_t ret = i2c_master_cmd_begin(i2c_port, cmd, 100 / portTICK_RATE_MS);
	i2c_cmd_link_delete(cmd);
	return ret;
}
/*!
 * @brief: The function is used as I2C bus read
 *
 *
 *
 *
 *	@param dev_addr : The device address of the sensor
 *	@param reg_addr : Address of the first register, where data is going to be read
 *	@param reg_data : This is the data read from the sensor, which is held in an array
 *	@param cnt : The no of bytes of data to be read
 *
 *
 *	@return status of the I2C read
 *
 *
 */
int8_t BMP280_I2C_Read(uint8_t dev_addr, uint8_t reg_addr, uint8_t *reg_data, uint8_t cnt) {
	i2c_cmd_handle_t cmd = i2c_cmd_link_create();
	i2c_master_start(cmd);
	i2c_master_write_byte(cmd, (dev_addr << 1) | I2C_MASTER_WRITE, ACK_CHECK_EN);
	i2c_master_write_byte(cmd, reg_addr, ACK_CHECK_EN);
	i2c_master_start(cmd);
	i2c_master_write_byte(cmd, (dev_addr << 1) | I2C_MASTER_READ, ACK_CHECK_EN);
	if (cnt > 1) {
		i2c_master_read(cmd, reg_data, cnt - 1, ACK);
	}
	i2c_master_read_byte(cmd, reg_data + cnt - 1, NACK);
	i2c_master_stop(cmd);
	esp_err_t ret = i2c_master_cmd_begin(i2c_port, cmd, 100 / portTICK_RATE_MS);
	i2c_cmd_link_delete(cmd);
	return ret;
}

int8_t BMP280_SPI_Write(uint8_t dev_addr, uint8_t reg_addr, uint8_t *reg_data, uint8_t cnt) {
	return ESP_FAIL;
}
int8_t BMP280_SPI_Read(uint8_t dev_addr, uint8_t reg_addr, uint8_t *reg_data, uint8_t cnt) {
	return ESP_FAIL;
}
