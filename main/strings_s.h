#pragma once

#ifndef MAX
#define MAX(x, y) (((x) > (y)) ? (x) : (y))
#endif

#ifndef MIN
#define MIN(x, y) (((x) < (y)) ? (x) : (y))
#endif

//cat non-overlapped strings
size_t strcat_s(char* dst, size_t dst_size, const char* src);
size_t strncat_s(char* dst, size_t dst_size, const char* src, size_t cnt);
size_t strcpy_s(char* dst, size_t dst_size, const char* src);
